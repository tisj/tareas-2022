# Cuestionario

## 1 - bash

Explique que hacen los siguientes comandos de bash:

- `ls`
- `cd ~`
- `mkdir`
- `mv`
- `cp`
- `pwd`
- `man`
- `cat`
- `less`
- `more`
- `head`
- `tail`

## 2 - git

¿que es git, para que sirve y como se usa?

## 3 - Variables de entorno

¿Que son las variables de entorno?
¿Que significa la variable de entorno PATH?
¿Cómo puedo establecer o modificar variables de entorno?

## 4 - Sysadmin

Describa lo que considera es la responsabilidad de un administrador de infraestructuras, así como las herramientas que es conveniente que sepa utilizar.

## 5 - Desarrollo

¿Considera útil para un desarrollador poseer habilidades relacionadas a la administración de infraestructuras?
De ser así, ¿Cómo cree que podrían ser aplicadas?, de lo contrario justifique su respuesta.
